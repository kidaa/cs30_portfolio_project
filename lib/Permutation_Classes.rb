class Word
  attr_accessor :word

  def initialize(word)
    @word = word.split(//)
  end

  def switch(num1, num2)
    first = @word[num1].dup
    second = @word[num2].dup
    @word[num2] = first
    @word[num1] = second
  end

  def to_s
    toprint = ""
    @word.each do |i|
      toprint << i
    end
    return toprint
  end
end

class Permuter

  def initialize(word)
      list_permutations(word)
  end

  def list_permutations(word)
    array = Word.new(word)
    rest = Word.new("")
    puts "\n"
    recursive_permuter(rest, array)
  end

  def recursive_permuter(rest, array)
    if array.word.length < 2
      puts "#{array}"
    elsif array.word.length == 2
      puts "#{rest}"+"#{array}"
      unless array.word[0] == array.word[1]
        array.switch(0,1)
        puts "#{rest}"+"#{array}"
        array.switch(0,1)
      end
    else
      l = array.word.length
      l.times do |i|
        unless array.word[0, i].include?(array.word[i])
          array.switch(0, i)
          rest.word << array.word.shift
          recursive_permuter(rest, array)
          array.word.unshift(rest.word.pop)
          array.switch(0, i)
        end
      end
    end
  end
end