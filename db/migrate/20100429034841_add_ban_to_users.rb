class AddBanToUsers < ActiveRecord::Migration
  def self.up
    change_table :users do |t|
	  t.boolean :ban, :default => false
	end
  end

  def self.down
    remove_column :users, :ban
  end
end
